---
title: "Hallucinations"
author: "Fosco Bernasconi"
date: "April 22, 2022"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Load libraries 
```{r, include=FALSE}
rm(list=ls(all=TRUE))
library(tidyverse)
library(broom)
library(lme4)
library(lmerTest) 
library(afex)
library(data.table)
library(sjPlot)
library(sjmisc)
library(devtools)
library(emmeans)
library(plotrix)
library(patchwork)
library(brms)
library(ordinal)
library(perm)
library(arsenal)
library(ComplexUpset)
library(ComplexHeatmap)
library(gtsummary)
library(here)
```

## Load data
```{r}

datapath = here()
dataname = '/Git/Phantom_border_PD/PB_hallucinations.csv'
dat = fread(paste(datapath,dataname,sep="/"),sep = "auto",header = T)

dat = dat %>% dplyr::select(PD,`Sub-group`,Gender,First_affected_side,Q1.Visual,Q2.AVH,Q3.Tactile,Q4.Olfactory, Q5.Gustatory, Q6.Vertigo,Q7a.PH1,Q7b.PH2,Q8.Visual_illusions,Q9.Passage,Q10.Delusions,Q11.PBS)

dat = dat %>% group_by(PD) %>% mutate(Q.PH = ifelse(Q7a.PH1 | Q7b.PH2 == 1, 1,0)) %>% ungroup()
dat = dat %>% group_by(PD) %>% mutate(sumH = sum(Q1.Visual,Q2.AVH,Q3.Tactile,Q.PH,Q8.Visual_illusions,Q9.Passage,Q11.PBS,na.rm=T)) %>% ungroup()

```

## Compute statistics for the clinical scores between patients:
```{r message=FALSE, warning=FALSE}

######## Statistics :
tmp <- dat %>% filter(!`Sub-group` == 'PD-nPH')
trial1 <- tmp %>% 
  dplyr::select(`Sub-group`,sumH)

tbl_summary(
  trial1,
  missing = "no", # don't list missing data separately
  by = `Sub-group`, # split table by group
  statistic = list(all_continuous() ~ "{mean} ({sd})"),
  type = list(sumH ~ "continuous")) %>%
  # add_n() %>% # add column with total number of non-missing observations
  # add_overall() %>% 
  add_p() %>% # test for a difference between groups
  modify_header(label = "**Variable**") %>% # update the column header
  bold_labels() 


######## Statistics :
tmp <- dat
trial1 <- tmp %>% 
  dplyr::select(`Sub-group`,Q1.Visual,Q2.AVH,Q8.Visual_illusions,Q9.Passage)

tbl_summary(
  trial1,
  missing = "no", # don't list missing data separately
  by = `Sub-group`, # split table by group
  statistic = list(all_continuous() ~ "{mean} ({sd})"),
  type = list(Q1.Visual ~ "categorical", Q2.AVH ~ "categorical",
              Q8.Visual_illusions~ "categorical",Q9.Passage~ "categorical")) %>%
  # add_n() %>% # add column with total number of non-missing observations
  # add_overall() %>% 
  add_p() %>% # test for a difference between groups
  modify_header(label = "**Variable**") %>% # update the column header
  bold_labels() 


tmp <- dat %>% filter(!`Sub-group` == 'PD-nPH')
trial1 <- tmp %>% 
  dplyr::select(`Sub-group`,Q1.Visual,Q2.AVH,Q8.Visual_illusions,Q9.Passage)

tbl_summary(
  trial1,
  missing = "no", # don't list missing data separately
  by = `Sub-group`, # split table by group
  statistic = list(all_continuous() ~ "{mean} ({sd})"),
  type = list(Q1.Visual ~ "categorical", Q2.AVH ~ "categorical",
             Q8.Visual_illusions~ "categorical",Q9.Passage~ "categorical")) %>%
  # add_n() %>% # add column with total number of non-missing observations
  # add_overall() %>% 
  add_p() %>% # test for a difference between groups
  modify_header(label = "**Variable**") %>% # update the column header
  bold_labels() 
```
