# Phantom boarder related to experimentally-induced presence hallucinations in Parkinson’s disease

- Blanke Olaf, Bernasconi Fosco, Potheegadoo Jevita 
- DOI: 
- email: olaf.blanke@epfl.ch


### Code and data

- ./Data: behavioral data (riPH_behavior.csv) and clinical-demographic data (Demographic.csv)
- ./Code: R script(s) to analyse behavioral and clinical-demographic data
